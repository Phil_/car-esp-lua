{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    # for the nodemcu-tool
    nodejs
  ];

  shellHook = ''
    npm install nodemcu-tool
  '';
}

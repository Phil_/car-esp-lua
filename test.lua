local servo_pin = 3
local base_freq = 100
local resolution = 10000

local max_pos = 19
local normal_pos = 15
local min_pos = 11

pwm2.stop()

cur_pos = normal_pos;

function handle_timer() 
    cur_pos = min_pos + ((cur_pos + 1 - min_pos) % (max_pos - min_pos));
    pwm2.set_duty(servo_pin, cur_pos);
end

local mytimer = tmr.create()
mytimer:register(500, tmr.ALARM_AUTO, handle_timer)

local steps = resolution / base_freq
pwm2.setup_pin_hz(servo_pin, base_freq, steps, normal_pos)

pwm2.start()
mytimer:start()
